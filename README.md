# AndroidUtils

## Useful utilities for Android Programming

### TextViewPlus
Adds new elements to the standard Android text view:

Element | Effect |
------- | ------ |
customFont | Allows you to set the font of a TextView to any font you have added in the assets. |


#### Usage

